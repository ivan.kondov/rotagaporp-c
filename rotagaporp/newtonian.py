""" Newtonian propagator for classical particle dynamics """

import numpy as np
import sympy as sp
from generic import spfloat
from propagators import PolynomialPropagator
from propagators import SymmetricPolynomialPropagator


def divdiff_1(x, y):
    """ compute divided differences for y_i=f(x_i) """
    n = len(x)
    a = []
    for i in range(n):
        a.append(y[i])
    for j in range(1, n):
        for i in range(n-1, j-1, -1):
            a[i] = (a[i]-a[i-1])/(x[i]-x[i-j])
    return np.array(a)


def divdiff_2(x, y, dtype=float, fpprec='fixed', prec=None):
    """ compute divided differences for y_i=f(x_i), alternative algorithm """
    n = len(x)
    assert len(x) == len(y)
    zero = {'fixed': dtype(0), 'multi': spfloat(0., prec)}
    a = np.full((n, n), zero[fpprec])
    a[:, 0] = y
    for i in range(1, n):
        for j in range(1, i+1):
            a[i][j] = (a[i][j-1]-a[i-1][j-1])/(x[i]-x[i-j])
    return np.diagonal(a)


def trial_points(number, dtype=float, fpprec='fixed', prec=None):
    """ construct the trial points for the algorithm from
        G. Ashkenazi et al. JCP 103, 10005 (1995) """
    zero = {'fixed': dtype(0), 'multi': spfloat(0., prec)}
    one = {'fixed': dtype(1), 'multi': spfloat(1., prec)}
    ci = {'fixed': 1j, 'multi': sp.N(1j, prec)}
    pi = {'fixed': np.pi, 'multi': sp.N(sp.pi, prec)}
    cosf = {'fixed': np.cos, 'multi': lambda x: sp.N(sp.cos(x), prec)}
    sinf = {'fixed': np.sin, 'multi': lambda x: sp.N(sp.sin(x), prec)}
    fspace = {
        'fixed': np.linspace,
        'multi': lambda l, h, n: np.arange(l, h+(h-l)/(n-1), (h-l)/(n-1))
    }
    center = zero[fpprec]

    if dtype == complex:
        # equally spaced points on an unit circle on the complex plane
        low, high = 0, 2*pi[fpprec]
        phisf = np.arange(low, high, (high-low)/number)
        circlef = lambda f: cosf[fpprec](f)+ci[fpprec]*sinf[fpprec](f)
        zsfun = np.vectorize(circlef)
        trials = zsfun(phisf)
    elif dtype == float:
        # equally spaced points in the interval [-1, 1]
        low, high = -one[fpprec], one[fpprec]
        trials = fspace[fpprec](low, high, number)
    else:
        raise ValueError('dtype must be complex or float')
    return trials.tolist(), center


def leja_points_ga(number, dtype=float, fpprec='fixed', prec=None):
    """ calculate Leja interpolation points based on an algorithm from
        G. Ashkenazi et al. JCP 103, 10005 (1995) """

    isclose = {'fixed': np.isclose,
               'multi': lambda x, y: abs(x-y) < 10**(-prec/2)}
    ntrials = {float: 3*number, complex: 2*number}
    trials, center = trial_points(ntrials[dtype], dtype, fpprec, prec)

    for index, trial in enumerate(trials):
        if isclose[fpprec](trial, center):
            del trials[index]
    chosen = [trials.pop(0)]
    while len(chosen) < number:
        prods = [np.prod(np.absolute(np.array(chosen)-t)) for t in trials]
        chosen.append(trials.pop(np.argmax(prods)))
    scaling = np.prod(np.absolute(center-np.array(chosen)))**(1./number)
    return np.array(chosen)/scaling, scaling


def leja_points_jb(nflp, dtype=float, fpprec='fixed', prec=None):
    """ calculate fast Leja points on an interval based on an algorithm from
        Baglama et al. Electronic Trans. Numer. Anal. 7, 124 (1998) """

    assert dtype == float
    ndtype = {'fixed': dtype, 'multi': np.object}
    scaling = {'fixed': dtype(1), 'multi': spfloat(1., prec)}
    left = {'fixed': dtype(-1), 'multi': spfloat(-1., prec)}
    right = {'fixed': dtype(1), 'multi': spfloat(1., prec)}

    zs = np.empty(nflp, dtype=ndtype[fpprec])
    zt = np.empty(nflp, dtype=ndtype[fpprec])
    zprod = np.empty(nflp, dtype=ndtype[fpprec])
    index = np.empty((nflp, 2), dtype=np.uint32)

    zt[0] = left[fpprec]
    zt[1] = right[fpprec]
    zt[2] = (zt[0] + zt[1])/2
    zs[0] = (zt[1] + zt[2])/2
    zs[1] = (zt[2] + zt[0])/2
    zprod[0] = np.prod(zs[0]-zt[:3])
    zprod[1] = np.prod(zs[1]-zt[:3])
    index[0, 0] = 1
    index[0, 1] = 2
    index[1, 0] = 2
    index[1, 1] = 0
    for i in range(3, nflp):
        maxi = np.argmax(np.abs(zprod[:i-1]))
        zt[i] = zs[maxi]
        index[i-1, 0] = i
        index[i-1, 1] = index[maxi, 1]
        index[maxi, 1] = i
        zs[maxi] = (zt[index[maxi, 0]]+zt[index[maxi, 1]])/2
        zs[i-1] = (zt[index[i-1, 0]]+zt[index[i-1, 1]])/2
        zprod[maxi] = np.prod(zs[maxi]-zt[:i])
        zprod[i-1] = np.prod(zs[i-1]-zt[:i])
        zprod[:i] = zprod[:i]*(zs[:i]-zt[i])
    return zt, scaling[fpprec]


def chebyshev_points(number):
    """ calculate Chebyshev interpolation points as roots of the Chebyshev
        polynomial of first kind """
    return np.array([np.cos((2*k+1)*np.pi/(2*number)) for k in range(number)])


def newton_scalar(lambdas, func, vals, scale=1.0, shift=0.0):
    """ Newton polynomial of a scalar function """
    ddif = divdiff_1(lambdas, func(scale*lambdas+shift))
    prod = np.full(len(vals), 1.)
    newt = np.full(len(vals), ddif[0])
    for k in range(1, len(lambdas)):
        prod *= vals/scale - shift/scale - lambdas[k-1]
        newt += ddif[k]*prod
    return newt


class Newtonian(PolynomialPropagator):
    """ Newtonian propagator for classical particle dynamics """
    name = 'newtonian'
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        lambdas, scaling = self.ip_points(algo='ga')
        scale = self.delta*scaling
        lamb_sc = lambdas*scale
        if self.substep is None:
            ddiffs = self.dd_coeffs(lambdas, lamb_sc, self.tstep)
        else:
            ddiffs = np.array([self.dd_coeffs(lambdas, lamb_sc, st)
                               for st in self.subtime])
        self.coeffs = ddiffs
        self.set_polynomial(system=self.syst, scale=1./scale, nodes=lambdas,
                            recursion='newton')

    def dd_coeffs(self, lamb, lamb_sc, t, algo=1):
        """ select the algorithm for divided differences """
        algo_map = {1: divdiff_1, 2: divdiff_2}
        if self.fpprec == 'fixed':
            retval = algo_map[algo](lamb, np.exp(lamb_sc*t))
        elif self.fpprec == 'multi':
            retval = algo_map[algo](lamb, [sp.exp(i) for i in lamb_sc*t])
        return retval

    def ip_points(self, dtype=float, algo='ga'):
        """ select the algorithm for interpolation points """
        algo_map = {'ga': leja_points_ga, 'jb': leja_points_jb,
                    'ch': chebyshev_points}
        return algo_map[algo](self.nterms, dtype=dtype, fpprec=self.fpprec,
                              prec=self.prec)


class SymmetricNewtonian(Newtonian, SymmetricPolynomialPropagator):
    """ Self-starting time-symmetric Newtonian propagator """

    name = 'symmetric newtonian'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        assert self.substep is None, 'substep not implemented'
        lambdas, scaling = self.ip_points(algo='ga')
        lamb_sc = lambdas*self.delta*scaling
        self.reverse_coeffs = self.dd_coeffs(lambdas, lamb_sc, -self.tstep)
        self.set_gvar_jacobian()
