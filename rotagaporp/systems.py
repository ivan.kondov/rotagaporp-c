""" Classes describing physical systems """
import sympy as sp
import numpy as np
from forcefield import FFList, PairFF
from pbc import get_distance_matrix_lc as get_distance_matrix
from pbc import wrap_positions
from generic import lambdify_long as lambdify
from generic import _float, spfloat, validate_numeric

class PhysicalSystem:
    """ base class for one-particle systems """
    def __init__(self, potential_energy, mass, q=sp.Symbol('q'),
                 p=sp.Symbol('p'), q0=None, p0=None, fpprec='fixed',
                 prec=None):
        self.q = q
        self.p = p
        assert fpprec in ['fixed', 'multi']
        self.q0 = q0 if fpprec == 'fixed' else spfloat(q0, prec)
        self.p0 = p0 if fpprec == 'fixed' else spfloat(p0, prec)
        self.mass = mass if fpprec == 'fixed' else spfloat(mass, prec)
        self.fpprec = fpprec
        self.prec = prec
        self.kinetic_energy = self.p**2/(2*self.mass)
        self.potential_energy = potential_energy
        self.hamiltonian = self.kinetic_energy + self.potential_energy
        self.force_expr = -sp.diff(self.potential_energy, self.q)

        self.force = _float(fpprec, self._force)
        self.energy = _float(fpprec, self._energy)
        self.get_potential_energy = _float(fpprec, self._get_potential_energy)
        self.get_kinetic_energy = _float(fpprec, self._get_kinetic_energy)

    def _force(self, q):
        return self.force_expr.subs(self.q, q)

    def _energy(self, q, p):
        return self.hamiltonian.subs({self.q: q, self.p: p})

    def _get_potential_energy(self, q):
        return self.potential_energy.subs(self.q, q)

    def _get_kinetic_energy(self, p):
        return self.kinetic_energy.subs(self.p, p)


class Harmonic(PhysicalSystem):
    """ harmonic oscillator """
    omega = 1.
    def __init__(self, mass=1., **kwargs):
        q = sp.Symbol('q')
        k = self.omega**2*mass
        potential_energy = k*q**2/2
        super().__init__(potential_energy, mass=mass, **kwargs)

    def solution(self, q0=None, p0=None, times=None):
        """ analytical solution of the equation of motion """
        if self.fpprec == 'fixed':
            from numpy import sin, cos
            from numpy import sqrt, arccos
            times = np.array(times, dtype=np.float)
        else:
            from sympy import sqrt
            from sympy import acos as arccos
            sin = lambda x: np.array(sp.Array(x).applyfunc(sp.sin))
            cos = lambda x: np.array(sp.Array(x).applyfunc(sp.cos))
            times = np.array(times, dtype=np.object)

        qini = self.q0 if q0 is None else q0
        pini = self.p0 if p0 is None else p0

        v0 = pini/self.mass
        A = sqrt((self.omega**2*qini**2+v0**2)/self.omega**2)
        phi = arccos(qini/A)
        q_ref = A*cos(self.omega*times+phi)
        p_ref = -A*self.omega*sin(self.omega*times+phi)*self.mass
        return q_ref, p_ref


class Anharmonic(PhysicalSystem):
    """ one particle in a double-well potential """
    def __init__(self, **kwargs):
        q = sp.Symbol('q')
        potential_energy = (1/2)*q**4 - q**2
        super().__init__(potential_energy, mass=1., **kwargs)


class Ballistic(PhysicalSystem):
    """ one particle in a potential with constant force """
    def __init__(self, force=1., **kwargs):
        self.cf = force
        q = sp.Symbol('q')
        potential_energy = self.cf*q
        super().__init__(potential_energy, mass=1., **kwargs)

    def solution(self, q0, p0, time):
        q_ref = -0.5*self.cf*time*time + p0*time + q0
        p_ref = - self.cf*time + p0
        return (q_ref, p_ref)


class LennardJones(PhysicalSystem):
    """ one particle in Lennard-Jones potential """
    def __init__(self, sigma=1., epsilon=1., mass=1., **kwargs):
        q = sp.Symbol('q')
        potential_energy = 4*epsilon*((sigma/q)**12-(sigma/q)**6)
        super().__init__(potential_energy, mass, **kwargs)


class Morse(PhysicalSystem):
    """ one particle in Morse potential """
    def __init__(self, qmin=1., kappa=1., d=1., mass=1., fpprec='fixed',
                 prec=None, **kwargs):
        self.qmin = qmin if fpprec == 'fixed' else spfloat(qmin, prec)
        self.kappa = kappa if fpprec == 'fixed' else spfloat(kappa, prec)
        self.d = d if fpprec == 'fixed' else spfloat(d, prec)
        q = sp.Symbol('q')
        expr = d*(sp.exp(-2*self.kappa*(q-self.qmin))
                  -2*sp.exp(-self.kappa*(q-self.qmin)))
        super().__init__(expr, mass, fpprec=fpprec, prec=prec, **kwargs)

    def solution(self, q0=None, p0=None, times=None):
        """ Analytic solution as published in Am. J. Phys. 46, 733 (1978) and
            Rev. Bras. Ensino Fis. 29(4), 543-547 (2007). The alternating
            sign in the expression for t0 is missing in these papers. """

        if self.fpprec == 'fixed':
            from numpy import exp, sqrt, log, sign
            from numpy import sin, cos, sinh, cosh
            from numpy import arccos, arccosh
            isclose = np.isclose
            times = np.array(times, dtype=np.float)
            resolution = np.finfo(float).resolution
        else:
            assert self.prec is not None, '"prec" parameter not defined'
            from sympy import exp, sqrt, sign
            from sympy import acos as arccos
            from sympy import acosh as arccosh
            sin = lambda x: np.array(sp.Array(x).applyfunc(sp.sin))
            cos = lambda x: np.array(sp.Array(x).applyfunc(sp.cos))
            log = lambda x: np.array(sp.Array(x).applyfunc(sp.log))
            sinh = lambda x: np.array(sp.Array(x).applyfunc(sp.sinh))
            cosh = lambda x: np.array(sp.Array(x).applyfunc(sp.cosh))
            resolution = 10**(-self.prec/2)
            isclose = lambda x, y: abs(x-y) < resolution
            times = np.array(times, dtype=np.object)

        qini = self.q0 if q0 is None else q0
        pini = self.p0 if p0 is None else p0
        assert qini > 0
        energy = self.energy(qini, pini)
        rho = (energy + self.d)/self.d
        assert rho >= 0
        omega0_sqr = 2.*self.kappa**2*self.d/self.mass
        direction = -sign(pini) if sign(pini) != 0 else 1
        yi = exp(self.kappa*(qini-self.qmin))
        if isclose(rho, 0.):
            q = np.full(times.shape, qini)
            p = np.full(times.shape, pini)
            return q, p
        elif isclose(rho, 1.):
            alpha = 4*self.d*self.kappa/self.mass
            sqrtarg = alpha/self.kappa*yi-alpha/(2*self.kappa)
            assert sqrtarg > 0 or abs(sqrtarg) < resolution
            t0 = times[0] + direction*2/alpha*sqrt(abs(sqrtarg))
            termq = log((1+omega0_sqr*(times-t0)**2)/2)
            termp = 2*omega0_sqr*(times-t0)/(omega0_sqr*(times-t0)**2 + 1)
        elif rho < 1:
            lambd = sqrt(omega0_sqr*(1.-rho))
            y1 = (self.d/abs(energy))*(1.+sqrt(rho))
            y2 = (self.d/abs(energy))*(1.-sqrt(rho))
            sqrtarg = (y1-yi)/(y1-y2)
            assert sqrtarg > 0 or abs(sqrtarg) < resolution
            t0 = times[0] + direction*2./lambd*arccos(sqrt(abs(sqrtarg)))
            termq = log((1.-sqrt(rho)*cos(lambd*(times-t0)))/(1.-rho))
            termp = (lambd*sqrt(rho)*sin(lambd*(times-t0))
                     /(1.-sqrt(rho)*cos(lambd*(times-t0))))
        else:
            gamma = sqrt(omega0_sqr*(rho-1.))
            y1 = 1./(1.-rho)*(1.-sqrt(rho))
            y2 = 1./(1.-rho)*(1.+sqrt(rho))
            sqrtarg = (yi-y2)/(y1-y2)
            assert sqrtarg > 0 or abs(sqrtarg) < resolution
            t0 = times[0] + direction*2./gamma*arccosh(sqrt(abs(sqrtarg)))
            termq = log((sqrt(rho)*cosh(gamma*(times-t0))-1.)/(rho-1.))
            termp = (gamma*sqrt(rho)*sinh(gamma*(times-t0))
                     /(sqrt(rho)*cosh(gamma*(times-t0))-1.))
        q = self.qmin + termq/self.kappa
        p = self.mass*termp/self.kappa
        return q, p


class ManyParticlePhysicalSystem:
    """ Generic many-particle physical system with optional periodic boundary
        conditions; in case of PBC only orthorhombic system supported """

    numeric = {'tool': 'subs', 'modules': None}
    def __init__(self, ff, q, p, mass, q0, p0, pbc=False, cell=None,
                 numeric=None, fpprec='fixed', prec=None):
        """ The masses are supplied for each degree of freedom. Modules
            supported by tool 'lambdify': math, mpmath, numpy, numexpr,
            tensorflow, sympy """

        self.fpprec = fpprec
        if numeric is not None:
            self.numeric = numeric
        validate_numeric(self.fpprec, self.numeric)

        if fpprec == 'fixed':
            dty = np.float64
            self.q0 = np.array(q0, dtype=dty)
            self.p0 = np.array(p0, dtype=dty)
            self.mass = np.array(mass, dtype=dty)
        elif fpprec == 'multi':
            fpfunc = lambda x: spfloat(x, prec)
            npfunc = np.vectorize(fpfunc, otypes=[np.object])
            self.q0 = npfunc(q0)
            self.p0 = npfunc(p0)
            self.mass = npfunc(mass)
        else:
            raise ValueError('invalid parameter: fpprec == '+fpprec)

        self.q = q
        self.p = p
        self.cell = np.array(cell)
        self.ff = ff
        assert self.mass.shape == self.q.shape == self.p.shape
        assert self.mass.shape == self.q0.shape == self.p0.shape
        self.kinetic_energy = sum(p**2/(2*m) for p, m in zip(self.p, self.mass))
        self.potential_energy = self.ff.get_potential_energy('cartesian')
        self.hamiltonian = (self.potential_energy, self.kinetic_energy)

        if hasattr(pbc, '__len__') and any(pbc):
            assert all(pbc), 'partial pbc not supported: ' + repr(pbc)
        self.pbc = all(pbc) if hasattr(pbc, '__len__') else pbc
        if self.pbc:
            assert self.cell is not None and len(self.cell.shape) == 1
        if isinstance(ff, list) and all(isinstance(f, PairFF) for f in ff):
            # these methods are valid only for pair potential forcefields
            if self.numeric['tool'] == 'theano':
                self.set_forces_theano()
                self.set_energy_theano()
            elif self.numeric['tool'] == 'lambdify':
                self.set_forces_lambdify()
                self.set_energy_lambdify()
            elif self.numeric['tool'] == 'subs':
                self.set_forces_subs()
                self.set_energy_subs()
            else:
                raise ValueError('not supported numeric: '+self.numeric['tool'])
        elif not self.pbc:
            # any forcefields but no PBC
            self.set_forces_nopbc()
            self.set_energy_nopbc()
        else:
            raise ValueError('PBC support only for pair potential forcefields')
        self.force = self.get_forces
        self.energy = self.get_total_energy

    def set_energy_nopbc(self):
        """ prepare energy expressions and functions in the case of any
            forcefields and without periodic boundary conditions """

        vq = self.potential_energy
        tp = self.kinetic_energy
        if self.numeric['tool'] == 'subs':
            vq_func = lambda q: vq.xreplace(dict(zip(self.q, q)))
            tp_func = lambda p: tp.xreplace(dict(zip(self.p, p)))
            self.get_potential_energy = _float(self.fpprec, vq_func)
            self.get_kinetic_energy = _float(self.fpprec, tp_func)
        elif self.numeric['tool'] == 'lambdify':
            vq_func = lambdify(self.q, vq, self.numeric['modules'])
            tp_func = lambdify(self.p, tp, self.numeric['modules'])
            self.get_potential_energy = lambda q: vq_func(q)
            self.get_kinetic_energy = lambda p: tp_func(p)
        elif self.numeric['tool'] == 'theano':
            from sympy.printing.theanocode import theano_function
            vq_func = theano_function(self.q, (vq, ))
            tp_func = theano_function(self.p, (tp, ))
            self.get_potential_energy = _float(self.fpprec, lambda q: vq_func(*q))
            self.get_kinetic_energy = _float(self.fpprec, lambda p: tp_func(*p))
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def get_total_energy(self, qval, pval):
        """ evaluate the total energy numerically """
        return self.get_potential_energy(qval)+self.get_kinetic_energy(pval)

    def get_energy_dists(self):
        """ the energy expression in terms of distance vectors components """
        vsymb = sum(f.pot_inte.xreplace({f.r: f.rc_cart}) for f in self.ff)
        funcs = (func for f in self.ff for func in f.rc_func)
        symbs = (sym for f in self.ff for sym in f.rc_symb)
        return vsymb.xreplace(dict(zip(funcs, symbs)))

    def set_energy_subs(self):
        """ prepare energy expression and substitutions in terms of distance
            vectors components """

        tp = self.kinetic_energy
        tp_func = lambda p: tp.xreplace(dict(zip(self.p, p)))
        self.get_kinetic_energy = _float(self.fpprec, tp_func)

        self.vrc_expr = self.get_energy_dists()
        self.dists = list(self.get_distance_symbols())
        self.get_potential_energy = self.get_potential_subs

    def get_potential_subs(self, qval):
        """ potential energy computed in terms of distance vectors components
            with sp.xreplace() """
        distv = self.get_distance_vectors(qval).flatten()
        dist_repl = dict(zip(self.dists, distv))
        auxxrepl = _float(self.fpprec, self.vrc_expr.xreplace)
        return auxxrepl(dist_repl)

    def set_energy_theano(self):
        """ compile a theano function to compute energy in terms of distance
            vectors components """
        from sympy.printing.theanocode import theano_function

        tp_func = theano_function(self.p, (self.kinetic_energy, ))
        self.get_kinetic_energy = lambda p: float(tp_func(*p))

        vrc_expr = self.get_energy_dists()
        vrc_func = theano_function(self.get_distance_symbols(), (vrc_expr, ))
        self.get_potential_energy = (
            lambda q: float(vrc_func(*self.get_distance_vectors(q).flatten()))
        )

    def set_energy_lambdify(self):
        """ compile a lambda function to compute energy in terms of distance
            vectors components """

        tp = self.kinetic_energy
        tp_func = lambdify(self.p, tp, self.numeric['modules'])
        self.get_kinetic_energy = lambda p: tp_func(p)

        vrc_expr = self.get_energy_dists()
        vrc_symb = self.get_distance_symbols()
        vrc_func = lambdify(vrc_symb, vrc_expr, self.numeric['modules'])
        self.get_potential_energy = (
            lambda q: vrc_func(self.get_distance_vectors(q).flatten())
        )

    def get_distance_symbols(self):
        """ construct a flat symbol list of the distance vector components """
        dista = self.get_distance_vectors_raw(np.array(self.q)).flatten()
        repll = (f.get_explicit_dists_repl() for f in self.ff)
        repld = {k: v for d in repll for k, v in d.items()}
        return (d.xreplace(repld) for d in dista)

    def get_forces_dists(self):
        """ forces expressions in terms of distance vectors components """
        from generic import get_all_partial_derivs
        from itertools import tee

        vsymb = sum(f.pot_inte.xreplace({f.r: f.rc_cart}) for f in self.ff)
        derivs = get_all_partial_derivs(-vsymb, self.q, 1)
        funcs = (func for f in self.ff for func in f.rc_func)
        valus1, valus2 = tee((v for f in self.ff for v in f.rc_valu), 2)
        symbs = (sym for f in self.ff for sym in f.rc_symb)
        replfv = dict(zip(funcs, valus1))
        replvs = dict(zip(valus2, symbs))
        return [d.xreplace(replfv).doit().xreplace(replvs) for d in derivs]

    def set_forces_nopbc(self):
        """ prepare forces expressions and functions in the general case of
            no periodic boundary conditions and general force fields """

        forces = -sp.derive_by_array(self.potential_energy, self.q)
        if self.numeric['tool'] == 'subs':
            self.get_forces = (
                lambda q: np.array(forces.xreplace(dict(zip(self.q, q))))
            )
        elif self.numeric['tool'] == 'lambdify':
            f_lambda = lambdify(self.q, forces, self.numeric['modules'])
            self.get_forces = lambda q: np.array(f_lambda(q))
        elif self.numeric['tool'] == 'theano':
            from sympy.printing.theanocode import theano_function
            if all(f == 0 for f in forces):
                self.get_forces = lambda q: np.zeros(len(q))
            elif all(f != 0 for f in forces):
                f_theano = theano_function((*self.q,), forces)
                self.get_forces = lambda q: np.array(f_theano(*q))
            else:
                raise ValueError('zero force expressions')
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def set_forces_subs(self):
        """ prepare forces expressions and substitutions in terms of distance
            vectors components """
        self.symbfors = self.get_forces_dists()
        self.dists = list(self.get_distance_symbols())
        self.get_forces = self.get_forces_subs

    def set_forces_theano(self):
        """ compile a theano function to compute forces in terms of distance
            vectors components """
        from sympy.printing.theanocode import theano_function as tf
        symbfors = self.get_forces_dists()
        if all(f == 0 for f in symbfors):
            self.get_forces = lambda q: np.zeros(len(q))
        else:
            self.forces_func = tf(self.get_distance_symbols(), symbfors)
            self.get_forces = self.get_forces_theano

    def set_forces_lambdify(self):
        """ compile a lambda function to compute forces in terms of distance
            vectors components """
        dists = self.get_distance_symbols()
        symbfors = self.get_forces_dists()
        self.forces_func = lambdify(dists, symbfors, self.numeric['modules'])
        self.get_forces = self.get_forces_lambdify

    def get_forces_subs(self, qval):
        """ forces computed in terms of distance vectors components with
            sp.xreplace() """
        distv = self.get_distance_vectors(qval).flatten()
        dist_repl = dict(zip(self.dists, distv))
        return np.array([f.xreplace(dist_repl) for f in self.symbfors])

    def get_forces_theano(self, qval):
        """ forces computed in terms of distance vectors components with
            a compiled theano function """
        distv = self.get_distance_vectors(qval).flatten()
        return np.array(self.forces_func(*distv))

    def get_forces_lambdify(self, qval):
        """ forces computed in terms of distance vectors components with
            a compiled lambda function """
        distv = self.get_distance_vectors(qval).flatten()
        return np.array(self.forces_func(distv))


class MPPSMD(ManyParticlePhysicalSystem):
    """ Generic multi-dimensional many-particle physical system with optional
        periodic boundary conditions; in case of PBC only orthorhombic
        system supported """

    def __init__(self, ffparams, symbols, masses, q0, p0, pbc=False,
                 cell=None, dim=None, **kwargs):
        """ ffparams: [{'class': str, 'pair': (), 'params': {}}] """

        Q0 = np.array(q0)
        P0 = np.array(p0)
        np_masses = np.array(masses)
        self.dim = dim if dim else Q0.shape(-1)
        assert len(np_masses.shape) == 1
        assert len(np_masses) == len(symbols)
        assert len(Q0.shape) == 2
        assert Q0.shape[0] == len(symbols)
        assert Q0.shape[1] == self.dim
        assert Q0.shape == P0.shape
        Q = sp.Array(sp.symbols(' '.join(['q'+s for s in symbols]), real=True))
        P = sp.Array(sp.symbols(' '.join(['p'+s for s in symbols]), real=True))
        # M = np.array([mxyz for mass in masses for mxyz in self.dim*[mass]])
        M = np_masses[..., np.newaxis]*np.full((self.dim,), 1)
        ffobj = FFList(ffparams, Q)

        if not hasattr(pbc, '__len__'):
            pbc = (pbc,) * self.dim
        if all(pbc):
            cell = np.array(cell)
            assert cell.shape == (self.dim, )
            self.get_distances = self.get_distances_pbc
            self.get_distance_vectors = self.get_distance_vectors_pbc
        elif not any(pbc):
            self.get_distances = self.get_distances_raw
            self.get_distance_vectors = self.get_distance_vectors_raw
        else:
            raise ValueError('partial pbc not supported: '+repr(pbc))

        syspar = {'q0': Q0.flatten(), 'p0': P0.flatten(), 'mass': M.flatten(),
                  'pbc': pbc, 'cell': cell}
        super().__init__(ffobj, Q, P, **syspar, **kwargs)

    def wrap_q(self, qval):
        """ wraps positions to unit cell (orthorhombic system only) """
        qval.shape = (-1, self.dim)
        q_wrap = wrap_positions(qval, self.cell)
        q_wrap.shape = (-1, )
        qval.shape = (-1, )
        return q_wrap

    def get_distances_pbc(self, qval):
        """ distances with pbc - returns a flat list of distances """
        dista = self.get_distance_vectors_pbc(qval)
        return np.linalg.norm(dista, axis=1).flatten()

    def get_distances_raw(self, qval):
        """ distances without pbc - returns a flat list of distances """
        dista = self.get_distance_vectors_raw(qval)
        return np.linalg.norm(dista, axis=1).flatten()

    def get_distance_vectors_raw(self, q):
        """ distance vectors without pbc - returns the lower triangle """
        qs = q.reshape((-1, self.dim))
        distm = np.array([np.subtract.outer(d, d) for d in qs.T]).T
        r, c = np.tril_indices(distm.shape[0], -1)
        return distm[r, c]

    def get_distance_vectors_pbc(self, q):
        """ distance vectors with pbc - returns the lower triangle """
        distm = get_distance_matrix(q.reshape((-1, self.dim)), self.cell)
        r, c = np.tril_indices(distm.shape[0], -1)
        return distm[r, c]


class MPPS1D(MPPSMD):
    """ Generic 1D many-particle physical system with optional PBC """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, dim=1, **kwargs)


class MPPS3D(MPPSMD):
    """ Generic 3D many-particle physical system with optional PBC """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, dim=3, **kwargs)


class ManyAtomsSystem(MPPS3D):
    """ Generic many-atoms system with optional periodic boundary conditions """

    def __init__(self, atoms, forcefield, **kwargs):
        """ current limitations: 1) only one type of pair interactions;
            2) only a single element is supported; 3) othorhombic system """
        from units import LJUnitsElement
        from ase.geometry.cell import is_orthorhombic, orthorhombic
        from itertools import combinations

        assert len(set(atoms.get_chemical_symbols())) == 1
        ljunits = LJUnitsElement(atoms.get_chemical_symbols()[0])
        positions = atoms.get_positions() / ljunits.sigmaA
        momenta = atoms.get_momenta() / ljunits.momentumA
        masses = atoms.get_masses() / ljunits.amass
        cell = atoms.get_cell() / ljunits.sigmaA
        if cell.shape == (3, 3):
            assert is_orthorhombic(cell)
            cell = orthorhombic(cell)
        else:
            assert cell.shape == (3,)

        pbc = atoms.get_pbc()
        symbols = [str(i)+'x:z' for i in range(len(atoms))]
        syspar = {'q0': positions, 'p0': momenta, 'symbols': symbols,
                  'masses': masses, 'pbc': pbc, 'cell': cell}

        indic = list(range(3*len(atoms)))
        pairs = list(combinations(zip(indic[0::3], indic[1::3], indic[2::3]), 2))
        ffparams = [{'class': forcefield, 'pair': p, 'params': {}} for p in pairs]
        super().__init__(ffparams, **syspar, **kwargs)
        self.atoms = atoms
