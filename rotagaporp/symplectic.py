""" Symplectic propagator for classical particle dynamics """
from propagators import Propagator

class Symplectic(Propagator):
    """ Symplectic propagator for classical particle dynamics """
    name = 'symplectic'
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.force_saved = self.syst.force(self.syst.q0)
        self.inv_mass = 1./self.syst.mass
    '''
    def step_slow(self, time, q0, p0):
        """ Simple velocity Verlet algorithm: 'slow' version """
        p_half = p0 + 0.5*self.tstep*self.syst.force(q0)
        qt = q0 + self.tstep*p_half/self.syst.mass
        pt = p_half + 0.5*self.tstep*self.syst.force(qt)
        return (qt, pt)
    '''
    def step_nopbc(self, time, q0, p0):
        """ Simple velocity Verlet algorithm: 'fast' version """
        p_half = p0 + 0.5*self.tstep*self.force_saved
        qt = q0 + self.tstep*p_half*self.inv_mass
        self.force_saved = self.syst.force(qt)
        pt = p_half + 0.5*self.tstep*self.force_saved
        return (qt, pt)

    def step_pbc(self, time, q0, p0):
        """ Simple velocity Verlet algorithm: 'fast' version with PBC """
        p_half = p0 + 0.5*self.tstep*self.force_saved
        qt = self.syst.wrap_q(q0 + self.tstep*p_half*self.inv_mass)
        self.force_saved = self.syst.force(qt)
        pt = p_half + 0.5*self.tstep*self.force_saved
        return (qt, pt)
