""" Three dimensional Lennard-Jones gas with two particles """
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import SymmetricChebyshev
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.mpliouville import MPLPQP
from rotagaporp.systems import MPPS3D, Morse

M = [2.0, 2.0]
numeric = {'tool': 'lambdify', 'modules': 'numpy'}
params = {'tstart': 0.0, 'tend': 1000., 'tstep': 0.1, 'tqdm': False}
syspar = {'symbols': ['0x:z', '1x:z'], 'q0': [[1, 2, 3], [4, 2, 3]],
          'p0': [[0, 0, 0], [0, 0, 0]], 'masses': M, 'numeric': numeric}
ffpars = [{'class': 'FFMorse', 'pair': ((0, 1, 2), (3, 4, 5)),
           'params': {'distance': 1.}}]
system = MPPS3D(ffpars, **syspar)

# velocity Verlet
vv_prop = Symplectic(syst=system, **params)
vv_prop.propagate()
time_vv, q_vv, p_vv = vv_prop.get_trajectory_3d()
r_vv = np.abs(q_vv[:, 1]-q_vv[:, 0])
r_vv_1p = np.linalg.norm(r_vv, axis=1)
pr_vv = (p_vv[:, 1]*M[0]-p_vv[:, 0]*M[1])/np.sum(M)
pr_vv_1p = np.einsum('ij,ij->i', pr_vv, r_vv/r_vv_1p[:, np.newaxis])
vv_prop.analyse()
print('energy drift of velocity verlet prop', np.max(np.fabs(vv_prop.er)))

system_1d = Morse(q0=3., p0=0.)
q_an, p_an = system_1d.solution(times=time_vv)
en0 = system_1d.energy(system_1d.q0, system_1d.p0)
evecf = np.vectorize(system_1d.energy)
err_an = np.array((evecf(q_an, p_an)-en0)/en0, dtype=float)
print('energy drift of analytic solution', np.max(np.fabs(err_an)))

# one-step propagator
params['liouville'] = MPLPQP
params['nterms'] = 4
ch_prop = Chebyshev(syst=system, **params)
ch_prop.propagate()
time_ch, q_ch, p_ch = ch_prop.get_trajectory_3d()
r_ch = np.abs(q_ch[:, 1]-q_ch[:, 0])
r_ch_1p = np.linalg.norm(r_ch, axis=1)
pr_ch = (p_ch[:, 1]*M[0]-p_ch[:, 0]*M[1])/np.sum(M)
pr_ch_1p = np.einsum('ij,ij->i', pr_ch, r_ch/r_ch_1p[:, np.newaxis])
ch_prop.analyse()
print('energy drift of one-step prop', np.max(np.fabs(ch_prop.er)))

# symmetrized propagator
params['numeric'] = numeric
ts_prop = SymmetricChebyshev(syst=system, **params)
ts_prop.propagate()
time_ts, q_ts, p_ts = ts_prop.get_trajectory_3d()
r_ts = np.abs(q_ts[:, 1]-q_ts[:, 0])
r_ts_1p = np.linalg.norm(r_ts, axis=1)
pr_ts = (p_ts[:, 1]*M[0]-p_ts[:, 0]*M[1])/np.sum(M)
pr_ts_1p = np.einsum('ij,ij->i', pr_ts, r_ts/r_ts_1p[:, np.newaxis])
ts_prop.analyse()
print('energy drift of symmetric prop', np.max(np.fabs(ts_prop.er)))

assert len(time_ts) == len(time_ch) == len(time_vv)
assert np.allclose(time_ts, time_ch)
assert np.allclose(time_ts, time_vv)

fig = plt.figure()
plot1 = fig.add_subplot(311)
plot1.plot(time_ch, np.maximum.accumulate(abs(ch_prop.er)),
           label='Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(ts_prop.er)),
           label='Symmetric Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(vv_prop.er)),
           label='Velocity Verlet')
plot1.set_ylabel('maximum energy drift')
plot1.set_yscale('log')
plot1.set_xscale('log')
plot1.legend()

plot2 = fig.add_subplot(312)
plot2.plot(time_ts, r_ch_1p, label='Chebyshev')
plot2.plot(time_ts, r_ts_1p, label='Symmetric Chebyshev')
plot2.plot(time_ts, r_vv_1p, label='Velocity Verlet')
plot2.plot(time_ts, q_an, label='Analytic solution')
plot2.set_xlabel(r'$t$')
plot2.set_ylabel(r'$r(t)$')
plot2.set_xscale('log')
plot2.legend()

plot3 = fig.add_subplot(313)
plot3.plot(time_ts, pr_ch_1p, label='Chebyshev')
plot3.plot(time_ts, pr_ts_1p, label='Symmetric Chebyshev')
plot3.plot(time_ts, pr_vv_1p, label='Velocity Verlet')
plot3.plot(time_ts, p_an, label='Analytic solution')
plot3.set_xlabel(r'$t$')
plot3.set_ylabel(r'$p_r(t)$')
plot3.set_xscale('log')
plot3.legend()

with open('pltobj.pkl', 'wb') as fh:
    pkl.dump(fig, fh)

fig.savefig('pltfig.png')

# plt.show()
