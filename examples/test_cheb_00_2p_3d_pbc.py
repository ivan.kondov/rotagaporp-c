""" Three dimensional ideal gas (no interactions) with two particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from rotagaporp.mpliouville import MPLPDV
from utilities.units import LJUnitsElement

argon_u = LJUnitsElement('Ar')
symbols = 'ArAr'
pbc = (True, True, True)
cell = np.array([5, 2, 2])*argon_u.sigmaA
positions = np.array([[0, 0, 0], [3, 0, 0]])*argon_u.sigmaA
momenta = np.array([[1, 0, 0], [-1, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, cell=cell, pbc=pbc, positions=positions, momenta=momenta)
syst = ManyAtomsSystem(atoms, 'FFZero', numeric={'tool': 'subs'})
params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'nterms': 4,
          'liouville': MPLPDV, 'pbc': True}
prop = Chebyshev(syst=syst, **params)
print('propagation begins')
prop.propagate()
prop.analyse()
(ti, qt, pt) = prop.get_trajectory_3d()

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'pbc': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory_3d()

fig = plt.figure()
plot = fig.add_subplot(211)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(ti, qt[:, 0, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plt.plot(ti, qt[:, 1, 0], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

plot = fig.add_subplot(212)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(ti, pt[:, 0, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plt.plot(ti, pt[:, 1, 0], label='p1 chebyshev')
plot.set_ylabel(r'$p_0,\quad p_1$')
plot.set_xlabel('time')
plt.legend()
plt.show()
