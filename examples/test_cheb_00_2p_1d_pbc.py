""" One dimensional ideal gas (with no interactions) with two particles """
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import MPPS1D
from rotagaporp.mpliouville import MPLPDV

syspar = {'symbols': ['0', '1'], 'q0': [[1], [3]], 'p0': [[1], [-1]],
          'masses': [1.0, 1.0], 'pbc': True, 'cell': [5.0],
          'numeric': {'tool': 'subs'}}
ffparams = [{'class': 'FFZero', 'pair': ((0,), (1,))}]
syst = MPPS1D(ffparams, **syspar)

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'nterms': 6,
          'liouville': MPLPDV, 'pbc': True}
prop = Chebyshev(syst=syst, **params)
print('propagation begins')
prop.propagate()
prop.analyse()
(ti, qt, pt) = prop.get_trajectory()

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'pbc': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory()

fig = plt.figure()
plot = fig.add_subplot(211)
plt.plot(tir, qtr[:, 0], label='q0 verlet')
plt.plot(ti, qt[:, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1], label='q1 verlet')
plt.plot(ti, qt[:, 1], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

plot = fig.add_subplot(212)
plt.plot(tir, ptr[:, 0], label='p0 verlet')
plt.plot(ti, pt[:, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1], label='p1 verlet')
plt.plot(ti, pt[:, 1], label='p1 chebyshev')
plot.set_xlabel('time')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()
plt.show()
