""" Test Chebyshev recursion coefficients C^(n)_k """
from rotagaporp.combinatorics import cnk_chebyshev_T1, cnk_chebyshev_T2
from rotagaporp.combinatorics import cnk_chebyshev_T3
from rotagaporp.combinatorics import cnk_chebyshev_U1, cnk_chebyshev_U2
from rotagaporp.combinatorics import cnk_chebyshev_U3

number = 32
for signed in (True, False):
    for n in range(0, number):
        for k in range(0, n+1):
            cnk1 = cnk_chebyshev_T1(n, k, signed)
            cnk2 = cnk_chebyshev_T2(n, k, signed)
            cnk3 = cnk_chebyshev_T3(n, k, signed)
            assert cnk1 == cnk2
            assert cnk1 == cnk3
            direct = cnk_chebyshev_U1(n, k, signed)
            recurs = cnk_chebyshev_U2(n, k, signed)
            summat = cnk_chebyshev_U3(n, k, signed)
            assert direct == recurs, 'n, k: '+repr(n)+' '+repr(k)
            assert direct == summat, 'n, k: '+repr(n)+' '+repr(k)
