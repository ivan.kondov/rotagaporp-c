""" Analysis of the terms of four-particle (iL)^n in three dimensions """
import yaml
import time
import numpy as np
from itertools import combinations
from rotagaporp.mpliouville import MPLPDV, get_stats_expr, get_stats_repl
from rotagaporp.systems import MPPS3D

nprtcls = 5
numeric = {'tool': 'subs'}

symbols = [str(i)+'x:z' for i in range(nprtcls)]
indic = list(range(3*nprtcls))
pairs = list(combinations(zip(indic[0::3], indic[1::3], indic[2::3]), 2))
ffparams = [{'class': 'FFLennardJones', 'pair': p, 'params': {}} for p in pairs]
masses = np.full((nprtcls, ), 1)
qval = np.full((nprtcls, 3), 1) # these are not valid
pval = np.full((nprtcls, 3), 0)
system = MPPS3D(ffparams, symbols, masses, qval, pval, numeric=numeric)

nterms = 3

ts = time.time()
iln = MPLPDV(nterms, system, numeric=numeric)
te = time.time()
print('preparation time:', te-ts)
print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))
print(yaml.dump(get_stats_repl(iln.repl, details=True)))
