""" Test non-recursive Newton interpolation for one particle in 1D """
import time
import sympy as sp
import numpy as np
from rotagaporp.liouville import SPLPNR, SPLPQP
from rotagaporp.systems import Harmonic, Anharmonic, Morse, LennardJones
from rotagaporp.newtonian import leja_points_ga

nterms = 3
syst = LennardJones()
qval = 5.
pval = -1.

lambdas, scaling = leja_points_ga(nterms)

ts = time.time()
lp_nr = SPLPNR(nterms, syst, recursion='newton', nodes=lambdas,
                          scale=1./scaling)
te = time.time()
print('preparation time for SPLPNR:', te-ts)

ts = time.time()
lp_qp = SPLPQP(nterms, syst, recursion='newton', nodes=lambdas,
                          scale=1./scaling)
te = time.time()
print("preparation time for SPLPQP:", te-ts)

assert all(nr.equals(qp) for nr, qp in zip(lp_nr.ilnpexpr, lp_qp.pobj.Lf))

qvec_nr, pvec_nr = lp_nr.get_val_float(qval, pval)
qvec_qp, pvec_qp = lp_qp.get_val_float(qval, pval)

assert np.allclose(qvec_nr, qvec_qp)
assert np.allclose(pvec_nr, pvec_qp)

qarr = np.random.rand(100)
parr = np.random.rand(100)
ts = time.time()
for q, p in zip(qarr, parr):
    lp_nr.get_val_float(q, p)
te = time.time()
print("propagation time for SPLPNR:", te-ts)

ts = time.time()
for q, p in zip(qarr, parr):
    lp_qp.get_val_float(q, p)
te = time.time()
print("propagation time for SPLPQP:", te-ts)
