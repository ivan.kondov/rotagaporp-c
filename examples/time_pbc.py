""" Performance of different PBC algorithms """
import numpy as np
from ase.geometry import get_distances
from ase.geometry import wrap_positions as wrap2
from rotagaporp.pbc import get_distance_matrix_lc
from rotagaporp.pbc import get_distance_matrix_bc
from rotagaporp.pbc import wrap_positions as wrap1
from utilities.timing import timeit

@timeit
def time_pbc1(posvec, cell):
    get_distance_matrix_lc(posvec, cell)

@timeit
def time_pbc2(posvec, cell):
    get_distance_matrix_bc(posvec, cell)

@timeit
def time_pbc3(posvec, cell):
    get_distances(posvec, cell=cell, pbc=True)

@timeit
def time_wrap1(posvec, cell):
    wrap1(posvec, cell=cell)

@timeit
def time_wrap2(posvec, cell):
    wrap2(posvec, cell=cell, pbc=True)

for npos in [1, 10, 100, 1000, 10000, 100000, 1000000]:
    print('Number of particles:', npos)
    cell = np.random.rand(3)
    posvec = np.random.rand(npos, 3)
    ase_cell=np.diag(cell)
    time_wrap1(posvec, cell)
    time_wrap2(posvec, ase_cell)

for npos in [1, 10, 100, 1000, 2000, 3000, 4000, 5000, 6000, 7000]:
    print('Number of particles:', npos)
    cell = np.random.rand(3)
    posvec = np.random.rand(npos, 3)
    ase_cell=np.diag(cell)
    time_pbc1(posvec, cell)
    time_pbc2(posvec, cell)
    time_pbc3(posvec, ase_cell)
