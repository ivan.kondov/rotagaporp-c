""" Analysis of the terms of three-particle (iL)^n in three dimensions """
import yaml
import sympy as sp
from rotagaporp.mpliouville import MPLPDR, get_stats_expr, get_stats_repl
from rotagaporp.forcefield import FFList
from rotagaporp import forcefield

nterms = 3

class System:
    symbols = ['0x:z', '1x:z']
    q = sp.Array(sp.symbols(' '.join(['q'+s for s in symbols]), real=True))
    p = sp.Array(sp.symbols(' '.join(['p'+s for s in symbols]), real=True))
    ffparams = [{'class': 'FFLennardJones', 'pair': ((0, 1, 2), (3, 4, 5))}]
    kinetic_energy = sum(pp**2/2 for pp in p)
    ff = FFList(ffparams, q)

system = System()

iln = MPLPDR(nterms, system, numeric={'tool': 'subs'}, variant='r')
# iln = MPLPDR(nterms, system, numeric={'tool': 'theano'}, variant='r')
print(iln.get_val_float([1, 2, 3, 1, 1, 1], [0, 0, 0, 1, 1, 1]))
print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))
print(yaml.dump(get_stats_repl(iln.repl, details=True)))

string = '{0:5d} {1:d}/{2:d} {3:6d} {4} {5}'
for symb, obj in zip(['Q', 'P'], [iln.ilnqexpr, iln.ilnpexpr]):
    print('--- iL^n {0} ---'.format(symb))
    print('power #var/total #terms type terms')
    for power, lst in enumerate(obj):
        for coord, expr in enumerate(lst):
            nterms = 1 if isinstance(expr, sp.Mul) else len(expr.args)
            terms = (expr,) if isinstance(expr, sp.Symbol) else expr.args
            print(string.format(power, coord+1, len(lst), nterms, type(expr), terms))
