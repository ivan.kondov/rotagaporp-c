""" Analysis of the terms of three-particle (iL)^n in three dimensions """
import yaml
import sympy as sp
import numpy as np
from rotagaporp.mpliouville import MPLPDV, get_stats_expr
from rotagaporp.systems import MPPS3D

nterms = 3

ffparams = [{'class': 'FFLennardJones', 'pair': ((0, 1, 2), (3, 4, 5))}]
symbols = ['0x:z', '1x:z']
masses = np.array([1, 1])
qval = np.array([[1, 2, 3], [1, 1, 1]])
pval = np.array([[0, 0, 0], [1, 1, 1]])
pbc = np.array((True, True, True))
cell = np.array((2, 2, 2))
# numeric={'tool': 'theano'}
numeric={'tool': 'subs'}
system = MPPS3D(ffparams, symbols, masses, qval, pval, pbc=pbc, cell=cell,
                numeric=numeric)

iln = MPLPDV(nterms, system, variant='r')
print(iln.get_val_float(system.q0, system.p0))
print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))

string = '{0:5d} {1:d}/{2:d} {3:6d} {4} {5}'
for symb, obj in zip(['Q', 'P'], [iln.ilnqexpr, iln.ilnpexpr]):
    print('--- iL^n {0} ---'.format(symb))
    print('power #var/total #terms type terms')
    for power, lst in enumerate(obj):
        for coord, expr in enumerate(lst):
            nterms = 1 if isinstance(expr, sp.Mul) else len(expr.args)
            terms = (expr,) if isinstance(expr, sp.Symbol) else expr.args
            print(string.format(power, coord+1, len(lst), nterms, type(expr), terms))
