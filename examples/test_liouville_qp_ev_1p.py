""" Numerical determination of Liouville eigenvalues for one particle """
import numpy as np
import matplotlib
matplotlib.use('Qt5Cairo')
import matplotlib.pyplot as plt
from rotagaporp.systems import Morse, Harmonic, Anharmonic, LennardJones
from rotagaporp.symplectic import Symplectic
from rotagaporp.chebyshev import Chebyshev

params = {'tstart': 0., 'tend': 100., 'tstep': 0.01, 'delta': 1., 'nterms': 5}
syspar = {'q0': 3., 'p0': 0.}
syst = Morse(**syspar)
# syst = Harmonic(**syspar)
# syst = Anharmonic(**syspar)
# syst = LennardJones(**syspar)
prop = Chebyshev(syst=syst, **params)
# prop = Symplectic(syst=syst, **params)
prop.propagate()
# prop.analyse()
(ti, qt, pt) = prop.get_trajectory()

half = len(ti)//2

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
ax1.plot(ti, qt, label='q')
ax1.plot(ti, pt, label='p')
ax1.set_xlabel(r'$t$')
ax1.set_ylabel(r'$q,\quad p$')
ax1.legend()

ax2.plot(qt, pt)
ax2.set_xlabel(r'$q$')
ax2.set_ylabel(r'$p$')

variable = pt
# '''
sigma = (params['tend']-params['tstart'])/3
center = (params['tstart']+params['tend'])/2
corrf = np.correlate(variable, variable, mode='same')
gauss = np.exp(-(ti-center)**2/(2*sigma**2))/(np.sqrt(2*np.pi)*sigma)
ax3.plot(ti, corrf*gauss)
ax3.set_xlabel(r'$\tau$')
ax3.set_ylabel(r'$C(\tau)=\int_{-\infty}^{+\infty}\ p(t+\tau)p(t)dt$')
fftf = np.fft.fft(corrf*gauss)
'''
cfft = np.fft.fft(variable)
fftf = cfft*np.conjugate(cfft)
'''
freq = np.fft.fftfreq(len(ti), d=params['tstep'])*2*np.pi

# ax4.plot(freq, fftf.real, label='Re(F)')
# ax4.plot(freq, fftf.imag, label='Im(F)')
ax4.plot(freq[:half], np.absolute(fftf[:half]), label=r'$|F(\omega)|$')
ax4.set_xlabel(r'$\omega$')
ax4.set_ylabel(r'$F(\omega)={\cal F} C(\tau)$')
ax4.legend()

fig.subplots_adjust(hspace=0.3)
plt.show()
