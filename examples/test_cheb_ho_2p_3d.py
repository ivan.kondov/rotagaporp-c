""" Two particles interacting in harmonic potential """
import sympy as sp
import numpy as np
import matplotlib.pyplot as plt
from ase.geometry import get_distances
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.systems import Harmonic, MPPS1D, MPPS3D
from rotagaporp.mpliouville import MPLPQP

params = {'tstart': 0.0, 'tend': 1.0, 'tstep': 0.1, 'nterms': 4,
          'liouville': MPLPQP, 'tqdm': True}

M = np.full(2, 2.0)
r0 = 3.5
kappa = sp.Rational(1, 2)
syspar_1d = {'symbols': ['0', '1'], 'q0': [[-1], [0]], 'p0': [[1], [0]],
             'masses': M}
ffpar_1d = [{'class': 'FFHarmonic', 'pair': ((0,), (1,)),
             'params': {'distance': r0, 'kappa': kappa}}]
syst_1d = MPPS1D(ffpar_1d, **syspar_1d)
prop_1d = Chebyshev(syst=syst_1d, **params)
prop_1d.propagate()
prop_1d.analyse()
(ti1d, qt1d, pt1d) = prop_1d.get_trajectory()

syspar_3d = {'symbols': ['0x:z', '1x:z'],
             'q0': [[1, 2, 3], [1.7071, 2.7071, 3]],
             'p0': [[0.7071, 0.7071, 0], [0, 0, 0]], 'masses': M}

ffpar_3d = [{'class': 'FFHarmonic', 'pair': ((0, 1, 2), (3, 4, 5)),
             'params': {'distance': r0, 'kappa': kappa}}]

syst_3d = MPPS3D(ffpar_3d, **syspar_3d)
prop_3d = Chebyshev(syst=syst_3d, **params)
prop_3d.propagate()
prop_3d.analyse()
(ti3d, qt3d, pt3d) = prop_3d.get_trajectory_3d()

# analytic solution of the equivalent 1p problem
sys_1p_par = {
    'q0': np.abs(syspar_1d['q0'][0][0]-syspar_1d['q0'][1][0])-r0,
    # from the motion of center of mass
    'p0': (M[0]*syspar_1d['p0'][1][0]+M[1]*syspar_1d['p0'][0][0])/np.sum(M),
    # from total kinetic energy = sum of particles's kinetic energies
    # 'p0': np.sqrt((M[0]*syspar_1d['p0'][1]**2+M[1]*syspar_1d['p0'][0]**2)/np.sum(M)),
    'mass': np.prod(M)/np.sum(M)
}
sys_1p = Harmonic(**sys_1p_par)
qrta = np.real(sys_1p.solution(sys_1p_par['q0'], sys_1p_par['p0'], ti1d)[0])

# 2p 1d distance
qrt1d = np.abs(qt1d[:, 0]-qt1d[:, 1])-r0

# 2p 3d distance
qrt3d = [get_distances(q)[1][0, 1]-r0 for q in qt3d]

# 2p 3d momenta
pt3d0 = np.linalg.norm(pt3d[:, 0], axis=1)
pt3d1 = np.linalg.norm(pt3d[:, 1], axis=1)

fig = plt.figure()
plot = fig.add_subplot(211)
plot.plot(ti1d, qrta, label='r(1p) analytic')
plot.plot(ti1d, qrt1d, label='r(1d) chebyshev')
plot.plot(ti3d, qrt3d, label='r(3d) chebyshev')
plot.set_ylabel(r'$r$')
plt.legend()

plot = fig.add_subplot(212)
plt.plot(ti3d, pt3d0, label=r'$|p_0|$ 3D')
plt.plot(ti1d, np.abs(pt1d[:, 0]), label=r'$|p_0|$ 1D')
plt.plot(ti3d, pt3d1, label=r'$|p_1|$ 3D')
plt.plot(ti1d, np.abs(pt1d[:, 1]), label=r'$|p_1|$ 1D')
plot.set_ylabel(r'$|p_0|,\quad |p_1|$')
plot.set_xlabel('time')
plt.legend()

plt.show()
