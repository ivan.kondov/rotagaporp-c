# Scalable propagators for classical molecular dynamics

See [this paper](https://doi.org/10.48550/arXiv.2302.03516) for further details.