from math import sqrt
from ase.units import _amu
from ase.units import second
from ase.data import atomic_numbers, atomic_masses

class LJUnits:
    """ LJ units; units ending with 'A' are relative to ASE units """
    def __init__(self):
        self.kB = 1.3806485e-23 # J.K^-1
        # self.aum = 1.660539040e-27 # kg 
        self.temp = self.epsilon/self.kB # K
        self.sigma = self.sigmaA*1e-10 # length, m
        self.mass = _amu*self.amass # kg
        self.tau = self.sigma*sqrt(self.mass/self.epsilon) # time, s
        self.force = self.epsilon/self.sigma # N = kg.m.s^-2
        self.stension = self.epsilon/self.sigma**2 # N / m
        self.pressure = self.epsilon/self.sigma**3 # Pa = N / m^2
        self.density = 1.0/self.sigma**3 # density, m^-3
        self.momentum = sqrt(self.mass*self.epsilon) # kg.m.s^-1
        self.tauA = self.tau*second
        self.momentumA = self.sigmaA*self.amass/self.tauA

class LJUnitsElement(LJUnits):
    """ LJ units for elements """

    sigmaA_dict = { # Angstrom
        'Ar': 3.405
    }
    epsilon_dict = { # Joule
        'Ar': 1.65e-21
    }

    def __init__(self, symbol):
        self.sigmaA = self.sigmaA_dict[symbol]
        self.epsilon = self.epsilon_dict[symbol]
        self.amass = atomic_masses[atomic_numbers[symbol]] # a.m.u.
        super().__init__()
