from pandasplot import PandasPlot
import pandas as pd

var_labels = {
    'totals.operations': 'number of operations',
    'totals.storage': 'number of objects',
    'preparation time': 'preparation time',
    'propagation time': 'propagation time',
    'chebyshev propagation time': 'propagation time',
    'class': 'MPLP class',
    'nparticles': 'N particles',
    'variant': 'variant',
    'delta': r'$\Delta L$',
    'nterms': r'Number of terms $N$',
    'tstep': r'$\Delta t$',
    'alphan': r'$\alpha/N$',
    'time step efficiency': r'$\Delta t/T_{runtime}$',
    'run time efficiency': r'$(t_{end}-t_{start})/T_{run time}$',
    'energy drift': r'$max(|\Delta E/ E|)$',
    'L^2 error': r'$max(|q-q_{ref}|)$',
    'propagator': 'propagator',
    'numeric tool': 'numeric tool',
    'max edrift': r'$\Delta\tilde{E}_{t_{end}}$'
}

files = [
#    'chebyshev_morse_high_highprec/results.json',
#    'chebyshev_morse_high_highprec/l2_error/results.json',
#    'chebyshev_morse_high_more/results.json',
#    'newtonian_morse_high_highprec/results.json',
#    'newtonian_morse_high_more/results.json',
#    'symplectic_morse_high/results.json',
#    'symplectic_morse_high_highprec/results.json',
#    'symplectic_morse_high_highprec/l2_error/results.json',
#    'chebyshev_anharmonic_high_delta_highprec/prec=30/results_merged.json',
#    'chebyshev_anharmonic_high_delta_highprec/prec=60/results_merged.json',
#    'chebyshev_anharmonic_high_delta_highprec/prec=60_reproduce/results_merged.json',
#    'chebyshev_morse_high_delta_highprec/prec=30/results_merged.json',
    'chebyshev_morse_high_delta_highprec/prec=60/tstep/results_merged.json',
#    'chebyshev_morse_high_delta_highprec/delta=100/results_merged.json',
#    'newtonian_morse_high_delta_highprec/new_sets/results_merged.json',
#    'chebyshev_lj_delta/tstep=0.01/results_merged.json',
#    'time_cheb_lj_2p_3d_theano_tstep/results_merged.json',
#    'time_verlet_lj_2p_3d_theano_tstep/results.json',
#    'time_cheb_lj_2p_3d_lambdify_tstep_highprec/results_merged.json',
#    'time_verlet_lj_2p_3d_lambdify_tstep_highprec/results.json',
#    'time_cheb_lj_np_3d_corr/results_merged.json',
#    'time_cheb_lj_np_3d_lambdify/results_merged.json',
#    'time_newt_lj_np_3d_lambdify/results_merged.json',
#    'time_iln_lj_np_3d_subs/results_merged.json',
#    'time_cheb_mo_2p_3d_lambdify_tstep_highprec/results_merged.json',
#    'time_verlet_mo_2p_3d_lambdify_tstep_highprec/results.json'
#    'chebyshev_morse_high_longtime_highprec/alldata.json'
]

dataframes = [pd.read_json(f, precise_float=True) for f in files]
df = pd.concat(dataframes, sort=False)

showx = 'nterms'
showy = 'energy drift'

# add first set
# select = {'class': 'MPLPDV', 'nparticles': 2}
# select = {'propagator': 'chebyshev', 'nterms': 30.}
select = {'propagator': 'chebyshev', 'tstep': 0.5}
# select = {'propagator': 'chebyshev', 'nterms': 10}
# select = {'propagator': 'chebyshev', 'class': 'MPLPAQ'}
# select = {'propagator': 'chebyshev', 'variant': 'dr', 'nparticles': 5}
# select = {'propagator': 'newtonian', 'tstep': 0.5}
# select = {'propagator': 'newtonian', 'delta': 12.}
# select = {'propagator': 'newtonian'}
# select = {'propagator': 'symplectic'}
# select = {'numeric tool': 'theano', 'tstep': 0.00001}
# select = {'variant': 'dv', 'nparticles': 5}
# select = {'variant': 'dv', 'nterms': 5}
# select = {'nparticles': 2}
# legendvars = ['propagator']
# legendvars = ['alphan']
# legendvars = ['tstep']
legendvars = ['delta']
# legendvars = ['nterms']
# legendvars = ['nparticles']
# legendvars = ['variant']
# legendvars = ['delta']
# legendvars = ['numeric tool']
# legendvars = ['class']
# legendvars = []

pplt1 = PandasPlot(df, showx=showx, showy=showy, labels=var_labels)
vary = {v: sorted(list(set(pplt1.df[v].dropna().tolist()))) for v in legendvars}
# pplt1.add_datasets(select=select, vary=vary, miny=1e-32, maxy=1, maxx=1)
# pplt1.add_datasets(select=select, vary=vary, minx=1e-3, maxx=10, miny=1e-10, maxy=1e4)
# pplt1.add_datasets(select=select, vary=vary, minx=1e-5, maxx=1e-2, miny=1e-10, maxy=1e4)
# pplt1.add_datasets(select=select, vary=vary, minx=2, miny=1e-16, maxy=100.)
# pplt1.add_datasets(select=select, vary=vary, minx=2)
pplt1.add_datasets(select=select, vary=vary)

# add second set
# select = {'propagator': 'chebyshev', 'class': 'MPLPNR', 'variant': 'dv'}
# select = {'numeric tool': 'lambdify', 'tstep': 0.00001}
# select = {'propagator': 'chebyshev', 'tstep': 1.}
# select = {'propagator': 'symplectic'}
# select = {'propagator': 'verlet'}
# select = {'variant': 'r', 'nparticles': 5}
# select = {'variant': 'r', 'nterms': 5}

# legendvars = ['nterms']
# legendvars = []
# vary = {v: sorted(list(set(pplt1.df[v].dropna().tolist()))) for v in legendvars}
# pplt1.add_datasets(select=select, vary=vary, marker='s', minx=1e-3, maxx=10)
# pplt1.add_datasets(select=select, vary=vary, marker='s', minx=1e-2, maxx=10)
# pplt1.add_datasets(select=select, vary=vary, marker='s', minx=1e-5, maxx=1e-2)
# pplt1.add_datasets(select=select, vary=vary, marker='s', minx=2)
# pplt1.add_datasets(select=select, vary=vary, marker='s')

# select = {'class': 'MPLPAQ', 'nterms': 5}
# select = {'class': 'MPLPAQ', 'nparticles': 5}
# pplt1.add_datasets(select=select, vary=vary, minx=2)
# select = {'variant': 'dr', 'nterms': 5}
# select = {'variant': 'dr', 'nparticles': 5}
# select = {'propagator': 'chebyshev', 'variant': 'dv', 'nparticles': 5}
# pplt1.add_datasets(select=select, vary=vary)
# select = {'propagator': 'chebyshev', 'variant': 'r', 'nparticles': 5}
# pplt1.add_datasets(select=select, vary=vary)

pplt2 = pplt1

# add further files
# df = pd.read_json('symplectic_morse_high_highprec/results.json',
#                    precise_float=True)
# pplt2 = PandasPlot(df, showx=showx, showy=showy, labels=var_labels,
#                    plotobj=pplt1.plotobj, colors=pplt1.colors)
# select = {'propagator': 'chebyshev', 'nterms': 4}
# legendvars = ['nterms']
# legendvars = ['variant']
# legendvars = ['class']
# vary = {v: sorted(list(set(pplt2.df[v].dropna().tolist()))) for v in legendvars}
# pplt2.add_datasets(select=select, vary=vary)
# select = {'propagator': 'chebyshev', 'nterms': 5, 'class': 'MPLPDV'}
# pplt2.add_datasets(select=select, vary=vary)


pplt2.set_axis_type(xtype='log', ytype='log')
pplt2.show(legend=True)
# pplt2.show()
