def def_float(fpprec):
    def def_float_fpprec(f):
        if fpprec == 'fixed':
            def func2(*args, **kwargs):
                return float(f(*args, **kwargs))
        else:
            func2 = f
        return func2
    return def_float_fpprec

@def_float('fixed')
def func(x):
    return x**2
    
print(func(2))
