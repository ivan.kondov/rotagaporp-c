import numpy as np
from pandas.io.json import json_normalize

statistics = []
stats = {}
en0 = np.array(0.5, dtype='float128')
ene = 0.5 - 1e-16*np.array(np.random.rand(10000), dtype='float128')
err = (ene-en0)/en0
absvals = np.fabs(err)
maxerr = np.max(absvals)
print(maxerr)
stats['maxerr'] = maxerr
statistics.append(stats)

df = json_normalize(statistics)
df.to_json('test_prec.json', double_precision=15)
